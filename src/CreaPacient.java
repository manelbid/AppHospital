import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

/**
 * 
 * @author iam20484311
 * @param
 * @return
 * 
 * Creates a new patient
 *
 */

public class CreaPacient extends JFrame implements ActionListener {
	
	static JDesktopPane desktopPane;
	JFormattedTextField textDni;
	JLabel dni;
	JLabel nom;
	JLabel cognom;
	JLabel nts;
	JTextField textNom;
	JTextField textCognom;
	JFormattedTextField textData;
	JFormattedTextField textNts;
	JButton valida;
	JButton cancel;
	Font font = new Font("Verdana", Font.BOLD, 14);

	public CreaPacient() throws ParseException {
		super("TaulerControl");

		desktopPane = new JDesktopPane();
		TaulerControl.frame.getContentPane().add(desktopPane);
		TaulerControl.frame.setVisible(true);

		nom = new JLabel("* Nom");
		nom.setBounds(90, 20, 140, 15);
		desktopPane.add(nom);

		cognom = new JLabel("* Cognom");
		cognom.setBounds(90, 50, 140, 15);
		desktopPane.add(cognom);

		dni = new JLabel("* DNI");
		dni.setBounds(90, 80, 200, 15);
		desktopPane.add(dni);

		JLabel data = new JLabel("* Data de naixement");
		data.setBounds(90, 110, 180, 15);
		desktopPane.add(data);

		nts = new JLabel("* Número targeta sanitària");
		nts.setBounds(90, 170, 200, 15);
		desktopPane.add(nts);

		JLabel estat = new JLabel("Estat del pacient");
		estat.setBounds(100, 200, 140, 15);
		desktopPane.add(estat);

		JLabel sexe = new JLabel("Sexe");
		sexe.setBounds(100, 140, 140, 15);
		desktopPane.add(sexe);

		JLabel doc = new JLabel("Doctor assignat");
		doc.setBounds(100, 230, 140, 15);
		desktopPane.add(doc);

		JLabel hab = new JLabel("Habitació assignada");
		hab.setBounds(100, 260, 180, 15);
		desktopPane.add(hab);

		JLabel alergies = new JLabel("Al·lèrgies a medicaments");
		alergies.setBounds(500, 20, 180, 15);
		desktopPane.add(alergies);

		JLabel intol = new JLabel("Intoleràncies alimentàries");
		intol.setBounds(750, 20, 200, 15);
		desktopPane.add(intol);

		JLabel obs = new JLabel("Observacions");
		obs.setBounds(100, 350, 140, 15);
		desktopPane.add(obs);

		JLabel ob = new JLabel("(* Dada obligatòria)");
		ob.setBounds(100, 650, 140, 15);
		desktopPane.add(ob);

		textNom = new JTextField();
		textNom.setBounds(300, 20, 140, 20);
		desktopPane.add(textNom);

		textCognom = new JTextField();
		textCognom.setBounds(300, 50, 140, 20);
		desktopPane.add(textCognom);

		textDni = new JFormattedTextField(new MaskFormatter("########U"));
		textDni.setBounds(300, 80, 140, 20);
		textDni.setToolTipText("00000000A");
		desktopPane.add(textDni);

		textData = new JFormattedTextField(new MaskFormatter("##/##/####"));
		textData.setBounds(300, 110, 140, 20);
		textData.setToolTipText("DD/MM/AAAA");
		desktopPane.add(textData);

		textNts = new JFormattedTextField(new MaskFormatter(
				"???? # ###### ## #"));
		textNts.setBounds(300, 170, 140, 20);
		textNts.setToolTipText("AAAA 0 000000 00 0");
		desktopPane.add(textNts);

		JComboBox<String> cEstat = new JComboBox<String>();
		cEstat.setBounds(300, 200, 140, 20);
		desktopPane.add(cEstat);
		cEstat.addItem("arribada");
		cEstat.addItem("tractament");
		cEstat.addItem("recuperació");
		cEstat.addItem("alta");

		JRadioButton m = new JRadioButton("M");
		m.setBounds(300, 140, 50, 15);
		desktopPane.add(m);

		JRadioButton f = new JRadioButton("F");
		f.setBounds(350, 140, 50, 15);
		desktopPane.add(f);

		ButtonGroup bSexe = new ButtonGroup();
		bSexe.add(m);
		bSexe.add(f);

		valida = new JButton("valida");
		valida.setBounds(750, 650, 100, 25);
		valida.addActionListener(this);
		desktopPane.add(valida);
		
		cancel = new JButton("cancel·la");
		cancel.setBounds(650, 650, 100, 25);
		cancel.addActionListener(this);
		desktopPane.add(cancel);

		JComboBox<String> docAssig = new JComboBox<String>();
		docAssig.setBounds(300, 230, 140, 20);
		desktopPane.add(docAssig);
		docAssig.addItem("Dr Calypso");
		docAssig.addItem("Dr Java");
		docAssig.addItem("Dr Andreu");
		docAssig.addItem("Dra Queen");

		JComboBox<Integer> habAssig = new JComboBox<Integer>();
		habAssig.setBounds(300, 260, 140, 20);
		desktopPane.add(habAssig);
		habAssig.addItem(1);
		habAssig.addItem(2);
		habAssig.addItem(3);
		habAssig.addItem(4);
		habAssig.addItem(5);
		habAssig.addItem(6);
		habAssig.addItem(7);
		habAssig.addItem(8);
		habAssig.addItem(9);
		habAssig.addItem(10);

		JCheckBox ai = new JCheckBox("antiinflamatoris");
		ai.setBounds(500, 50, 140, 15);
		desktopPane.add(ai);

		JCheckBox ab = new JCheckBox("antibiòtics");
		ab.setBounds(500, 80, 140, 15);
		desktopPane.add(ab);

		JCheckBox ac = new JCheckBox("anticoagulants");
		ac.setBounds(500, 110, 140, 15);
		desktopPane.add(ac);

		JCheckBox acv = new JCheckBox("anticonvulsius");
		acv.setBounds(500, 140, 140, 15);
		desktopPane.add(acv);

		JCheckBox ins = new JCheckBox("insulina");
		ins.setBounds(500, 170, 140, 15);
		desktopPane.add(ins);

		JCheckBox con = new JCheckBox("contrasts");
		con.setBounds(500, 200, 140, 15);
		desktopPane.add(con);

		JCheckBox al = new JCheckBox("altres");
		al.setBounds(500, 230, 140, 15);
		desktopPane.add(al);

		JCheckBox gl = new JCheckBox("gluten");
		gl.setBounds(750, 50, 140, 15);
		desktopPane.add(gl);

		JCheckBox la = new JCheckBox("lactosa");
		la.setBounds(750, 80, 140, 15);
		desktopPane.add(la);

		JCheckBox ou = new JCheckBox("ou");
		ou.setBounds(750, 110, 140, 15);
		desktopPane.add(ou);

		JCheckBox pr = new JCheckBox("proteïna de llet");
		pr.setBounds(750, 140, 140, 15);
		desktopPane.add(pr);

		JCheckBox ll = new JCheckBox("llevat");
		ll.setBounds(750, 170, 140, 15);
		desktopPane.add(ll);

		JCheckBox alc = new JCheckBox("alcohol");
		alc.setBounds(750, 200, 140, 15);
		desktopPane.add(alc);

		JCheckBox hi = new JCheckBox("histamines");
		hi.setBounds(750, 230, 140, 15);
		desktopPane.add(hi);

		JCheckBox alt = new JCheckBox("altres");
		alt.setBounds(750, 260, 140, 15);
		desktopPane.add(alt);

		JScrollPane sp = new JScrollPane();
		sp.setBounds(100, 380, 800, 250);
		desktopPane.add(sp);

		JTextArea ta = new JTextArea();
		ta.setBounds(100, 380, 800, 250);
		ta.setLineWrap (true);
		sp.setViewportView(ta);

	}

	/**
	 * Validate ID card character and empty fields
	 */
	@Override
	public void actionPerformed(ActionEvent valid) {
		if (valid.getSource() == valida) {
			String lletres = "TRWAGMYFPDXBNJZSQVHLCKE";
			try {
				int residu = Integer
						.parseInt(textDni.getText().substring(0, 8)) % 23;
				char nif = lletres.charAt(residu);
				if (nif != textDni.getText().charAt(8)) {
					dni.setText("* DNI            incorrecte");
					dni.setForeground(Color.RED);
					textDni.setBorder(BorderFactory.createLineBorder(Color.red));
				} else {
					dni.setText("* DNI");
					dni.setForeground(null);
				}
			} catch (Exception exc) {

			}
			if (textNom.getText().equals("")) {
				nom.setForeground(Color.RED);
				textNom.setBorder(BorderFactory.createLineBorder(Color.RED));
			} else {
				nom.setForeground(null);
				textNom.setBorder(BorderFactory.createLineBorder(Color.GRAY));
			}
			if (textCognom.getText().equals("")) {
				cognom.setForeground(Color.RED);
				textCognom.setBorder(BorderFactory.createLineBorder(Color.RED));
			} else {
				cognom.setForeground(null);
				textCognom.setBorder(BorderFactory.createLineBorder(Color.GRAY));
			}
		}
		else if (valid.getSource() == cancel){
			TaulerControl.frame.remove(desktopPane);
			carregaXML chx = new carregaXML();
		}
	}
}
