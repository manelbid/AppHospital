import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.table.DefaultTableModel;

/**
 * 
 * @author iam20484311
 * @param
 * @return
 * 
 * Manages the main board
 *
 */

public class TaulerControl implements ActionListener {

	JMenuItem about;
	JMenuItem ajuda;
	JMenuItem creaPacient;
	JMenuItem visites;
	JButton dades;
	JButton visit;
	JButton medic;
	Ajuda ajudaP;
	Visites visita;
	boolean vActiu = false;
	boolean pActiu = false;
	boolean aActiu = false;
	boolean mActiu = false;
	DefaultTableModel model;
	
	public static JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TaulerControl window = new TaulerControl();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TaulerControl() {
		initialize();
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(300, 300, 1150, 800);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		JToolBar toolBar = new JToolBar(JToolBar.VERTICAL);
        frame.getContentPane().add(toolBar, BorderLayout.WEST);
         
        JLabel logo = new JLabel();
        logo.setIcon(new ImageIcon("/images/hospital-icon.gif"));
        logo.setBackground(Color.WHITE);
        toolBar.add(logo, BorderLayout.NORTH);
        
        frame.setTitle("APLICACIÓ MÈDICA");
        
        dades = new JButton("<html>Dades<br>pacients</html>");
        dades.setIcon(new ImageIcon(TaulerControl.class.getResource("/images/pacient.png")));
        dades.addActionListener(this);
        toolBar.add(dades);
        
        visit = new JButton("<html>Visites<br>metge</html>");
        visit.setIcon(new ImageIcon(TaulerControl.class.getResource("/images/visita.png")));
        visit.addActionListener(this);
        toolBar.add(visit);
        
        medic = new JButton("<html>Medicació</html>");
        medic.setIcon(new ImageIcon(TaulerControl.class.getResource("/images/medic.png")));
        medic.addActionListener(this);
        toolBar.add(medic);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenu mnGesti = new JMenu("Dades Pacients");
		menuBar.add(mnGesti);
		
		creaPacient = new JMenuItem("Crea pacient");
		mnGesti.add(creaPacient);
		creaPacient.addActionListener(this);
		
		JMenuItem mntmModificarInformaci = new JMenuItem("Modifica informació");
		mnGesti.add(mntmModificarInformaci);
		
		JMenuItem mntmDesactivarPacient = new JMenuItem("Desactiva pacient");
		mnGesti.add(mntmDesactivarPacient);
		
		JMenuItem mntmCercaDePacient = new JMenuItem("Cerca de pacient");
		mnGesti.add(mntmCercaDePacient);
		
		JMenuItem mntmAssignaHabitaci = new JMenuItem("Assigna habitació");
		mnGesti.add(mntmAssignaHabitaci);

		JMenu mnVisites = new JMenu("Visites Metge");
		menuBar.add(mnVisites);
		
		visites = new JMenuItem("Programa visita");
		mnVisites.add(visites);
		visites.addActionListener(this);

		JMenu mnMedicaci = new JMenu("Medicació");
		menuBar.add(mnMedicaci);

		JMenu mnAtencionsPacients = new JMenu("Atencions Pacients");
		menuBar.add(mnAtencionsPacients);

		JMenu mnParmetres = new JMenu("Paràmetres");
		menuBar.add(mnParmetres);

		JMenu mnAjuda = new JMenu("Ajuda");
		menuBar.add(mnAjuda);

		ajuda = new JMenuItem("Ajuda");
		mnAjuda.add(ajuda);
		ajuda.addActionListener(this);

		about = new JMenuItem("Sobre...");
		mnAjuda.add(about);
		about.addActionListener(this);

	}
	
	/**
	 * Execute each new panel depending on which button is pressed
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == about) {
			JOptionPane.showMessageDialog(null, "Manel Muñoz, 2 WIAM\n© 2014", "About", 
					JOptionPane.INFORMATION_MESSAGE, new ImageIcon("src/images/hospital-icon.gif"));
		} else if (e.getSource() == visit) {
			if (pActiu) {
				TaulerControl.frame.remove(carregaXML.dPane);
			} else if (mActiu) {
				TaulerControl.frame.remove(Medicaments.mPane);
			}
			visita = new Visites();
			model = (DefaultTableModel) Visites.taula.getModel();
			int f = Visites.taula.getRowCount();
			for (int i = 0; f > i; i++) {
				model.removeRow(0);
			}
			TaulerControl.frame.remove(Visites.vPane);
			visita = new Visites();
			vActiu = true;
			visita.validate();
			visita.repaint();
			visit.setEnabled(false);
			dades.setEnabled(true);
			medic.setEnabled(true);
		} else if (e.getSource() == ajuda) {
			if (pActiu) {
				TaulerControl.frame.remove(carregaXML.dPane);
			} else if (vActiu) {
				TaulerControl.frame.remove(Visites.vPane);
			}
			ajudaP = new Ajuda();
			aActiu = true;
			ajudaP.setEnabled(true);
		} else if (e.getSource() == dades){
			if (vActiu) {
				TaulerControl.frame.remove(Visites.vPane);
			} else if (mActiu) {
				TaulerControl.frame.remove(Medicaments.mPane);
			}
			
			carregaXML chx = new carregaXML();
			model = (DefaultTableModel) carregaXML.taula.getModel();
			int f = carregaXML.taula.getRowCount();
			for (int i = 0; f > i; i++) {
				model.removeRow(0);
			}
			TaulerControl.frame.remove(carregaXML.dPane);
			chx = new carregaXML();
			pActiu = true;
			chx.validate();
			chx.repaint();
			dades.setEnabled(false);
			visit.setEnabled(true);
			medic.setEnabled(true);
		} else if (e.getSource() == medic) {
			if (pActiu) {
				TaulerControl.frame.remove(carregaXML.dPane);
			} else if (vActiu) {
				TaulerControl.frame.remove(Visites.vPane);
			}
			Medicaments med = new Medicaments();
			mActiu = true;
			dades.setEnabled(true);
			visit.setEnabled(true);
			medic.setEnabled(false);
			med.validate();
			med.repaint();
			
		}
	}
}
