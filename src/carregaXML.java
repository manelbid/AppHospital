import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.ParseException;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class carregaXML extends JFrame implements ActionListener {

	JButton elimina;
	JButton afegeix;
	static JDesktopPane dPane;
	static Vector fila;
	static Vector files = new Vector();
	static JTable taula;

	carregaXML() {

		super("TaulerControl");

		try {
			File arxiu = new File("dades.xml");
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(arxiu);
			doc.getDocumentElement().normalize();
			NodeList nodeLst = doc.getElementsByTagName("pacient");

			for (int s = 0; s < nodeLst.getLength(); s++) {
				fila = new Vector();
				files.add(fila);
				Node fstNode = nodeLst.item(s);

				if (fstNode.getNodeType() == Node.ELEMENT_NODE) {

					Element fstElmnt = (Element) fstNode;

					String cognom1 = getTextValue(fstElmnt, "cognom1");
					String cognom2 = getTextValue(fstElmnt, "cognom2");
					fila.add(cognom1 + " i " + cognom2);

					String nom = getTextValue(fstElmnt, "nom");
					fila.add(nom);

					String dni = getTextValue(fstElmnt, "dni");
					fila.add(dni);

					String datanac = getTextValue(fstElmnt, "datanac");
					fila.add(datanac);

					String targeta = getTextValue(fstElmnt, "targeta");
					fila.add(targeta);

					int estat = getIntValue(fstElmnt, "estat");
					fila.add(estat);

					NodeList allergiesNodeList = fstElmnt
							.getElementsByTagName("allergies");

					if (allergiesNodeList != null
							&& allergiesNodeList.getLength() > 0) {
						for (int i = 0; i < allergiesNodeList.getLength(); i++) {
							Element allergiesElement = (Element) allergiesNodeList
									.item(i);
							if (getIntValue(allergiesElement,
									"antiinflamatoris") == 1)
								fila.add("Antiinflamatoris");
							if (getIntValue(allergiesElement, "antibiotics") == 1)
								fila.add("Antibiòtics");
							if (getIntValue(allergiesElement, "anticoagulants") == 1)
								fila.add("Anticoagulants");
							if (getIntValue(allergiesElement, "anticonvulsius") == 1)
								fila.add("Anticonvulsius");
							if (getIntValue(allergiesElement, "insulina") == 1)
								fila.add("Insulina");
							if (getIntValue(allergiesElement, "contrasts") == 1)
								fila.add("Contrasts");
							NodeList altresallergies = allergiesElement
									.getElementsByTagName("altres");
							String altresal = null;
							if (altresallergies.getLength() > 0) {
								Node n = altresallergies.item(0);
								Node fill = n.getFirstChild();
								if (fill != null) {
									altresal = fill.getTextContent();
								}
							}
							
						}
					}
					NodeList intoleranciesNodeList = fstElmnt
							.getElementsByTagName("intolerancies");

					if (intoleranciesNodeList != null
							&& intoleranciesNodeList.getLength() > 0) {
						for (int i = 0; i < intoleranciesNodeList.getLength(); i++) {
							Element intoleranciesElement = (Element) intoleranciesNodeList
									.item(i);
							if (getIntValue(intoleranciesElement, "gluten") == 1)
								fila.add("Gluten");
							if (getIntValue(intoleranciesElement, "lactosa") == 1)
								fila.add("Lactosa");
							if (getIntValue(intoleranciesElement, "ou") == 1)
								fila.add("Ou");
							if (getIntValue(intoleranciesElement, "protllet") == 1)
								fila.add("Proteïna de llet");
							if (getIntValue(intoleranciesElement, "llevat") == 1)
								fila.add("Llevat");
							if (getIntValue(intoleranciesElement, "alcohol") == 1)
								fila.add("Alcohol");
							if (getIntValue(intoleranciesElement, "histamines") == 1)
								fila.add("Histamines");
							NodeList altresintolerancies = intoleranciesElement
									.getElementsByTagName("altresi");
							String altresin = null;
							if (altresintolerancies.getLength() > 0) {
								Node n = altresintolerancies.item(0);
								Node fill = n.getFirstChild();
								if (fill != null) {
									altresin = fill.getTextContent();
								}
							}
							
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		dPane = new JDesktopPane();
		dPane.setLayout(new BorderLayout());
		TaulerControl.frame.getContentPane().add(dPane);
		TaulerControl.frame.setVisible(true);

		elimina = new JButton("elimina");
		elimina.addActionListener(this);
		elimina.setBounds(850, 650, 100, 25);
		dPane.add(elimina);

		afegeix = new JButton("afegeix");
		afegeix.addActionListener(this);
		afegeix.setBounds(650, 650, 100, 25);
		dPane.add(afegeix);

		JButton modifica = new JButton("modifica");
		modifica.addActionListener(this);
		modifica.setBounds(750, 650, 100, 25);
		dPane.add(modifica);

		Vector columnes = new Vector();

		columnes.add("Cognoms");
		columnes.add("Nom");
		columnes.add("DNI");
		columnes.add("Data Naixement");
		columnes.add("Targeta");
		columnes.add("estat");
		columnes.add("Al·lèrgies");
		columnes.add("Intoleràncies");

		taula = new JTable(files, columnes);
		dPane.add(taula);
		JScrollPane panel = new JScrollPane(taula);
		dPane.add(panel, BorderLayout.CENTER);

	}

	/**
	 * I take a xml element and the tag name, look for the tag and get the text
	 * content i.e for <employee><name>John</name></employee> xml snippet if the
	 * Element points to employee node and tagName is 'name' I will return John
	 */
	public static String getTextValue(Element ele, String tagName) {
		String textVal = null;
		NodeList nl = ele.getElementsByTagName(tagName);
		if (nl != null && nl.getLength() > 0) {
			Element el = (Element) nl.item(0);
			if (el.getFirstChild().getNodeValue() != null)
				textVal = el.getFirstChild().getNodeValue();
		}

		return textVal;
	}

	/**
	 * Calls getTextValue and returns a int value
	 */
	public static int getIntValue(Element ele, String tagName) {
		// in production application you would catch the exception
		return Integer.parseInt(getTextValue(ele, tagName));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == elimina) {
			DefaultTableModel model = (DefaultTableModel) taula.getModel();
			model.removeRow(taula.getSelectedRow());
		} else if (e.getSource() == afegeix) {
			CreaPacient cp;
			try {
				cp = new CreaPacient();
				DefaultTableModel model2 = (DefaultTableModel) taula.getModel();
				int f = taula.getRowCount();
				for (int i = 0; f > i; i++) {
					model2.removeRow(0);
				}
				TaulerControl.frame.remove(dPane);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}

	}

}
