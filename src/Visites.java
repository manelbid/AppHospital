import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * 
 * @author iam20484311
 * @param
 * @return
 * 
 *         Shows the scheduled visits
 *
 */

public class Visites extends JFrame implements ActionListener {

	JButton elimina;
	JButton afegeix;
	static JDesktopPane vPane;
	static Vector fila = new Vector();
	static Vector files = new Vector();
	static JTable taula;

	public Visites() {
		super("TaulerControl");

		try {
			File arxiu = new File("dades2.xml");
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(arxiu);
			doc.getDocumentElement().normalize();
			NodeList nodeLst = doc.getElementsByTagName("visita");

			for (int s = 0; s < nodeLst.getLength(); s++) {
				fila = new Vector();
				files.add(fila);
				Node fstNode = nodeLst.item(s);

				if (fstNode.getNodeType() == Node.ELEMENT_NODE) {

					Element fstElmnt = (Element) fstNode;

					String cognom1 = getTextValue(fstElmnt, "cognom1");
					String cognom2 = getTextValue(fstElmnt, "cognom2");
					String nom = getTextValue(fstElmnt, "nom");
					fila.add(cognom1 + " i " + cognom2 + ", " + nom);
					
					String nomMet = getTextValue(fstElmnt, "metge");
					fila.add(nomMet);
					
					String hora = getTextValue(fstElmnt, "hora");
					fila.add(hora);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		vPane = new JDesktopPane();
		vPane.setLayout(new BorderLayout());
		TaulerControl.frame.getContentPane().add(vPane);
		TaulerControl.frame.setVisible(true);

		elimina = new JButton("elimina");
		elimina.addActionListener(this);
		elimina.setBounds(850, 650, 100, 25);
		vPane.add(elimina);

		afegeix = new JButton("afegeix");
		afegeix.addActionListener(this);
		afegeix.setBounds(650, 650, 100, 25);
		vPane.add(afegeix);

		JButton modifica = new JButton("modifica");
		modifica.addActionListener(this);
		modifica.setBounds(750, 650, 100, 25);
		vPane.add(modifica);

		JLabel ord = new JLabel("Ordena per:");
		ord.setBounds(50, 650, 140, 20);
		vPane.add(ord);

		JComboBox<String> ordena = new JComboBox<String>();
		ordena.setBounds(150, 650, 80, 20);
		vPane.add(ordena);
		ordena.addItem("");
		ordena.addItem("Pacient");
		ordena.addItem("Metge");
		ordena.addItem("Data");

		JLabel filt = new JLabel("Filtre:");
		filt.setBounds(300, 650, 140, 20);
		vPane.add(filt);

		JComboBox<String> filtre = new JComboBox<String>();
		filtre.setBounds(350, 650, 180, 20);
		vPane.add(filtre);
		filtre.addItem("");
		filtre.addItem("Pacients sense metge");
		filtre.addItem("Metges sense pacient");

		Vector columnes = new Vector();

		columnes.add("Pacients");
		columnes.add("Metges");
		columnes.add("Data i Hora");

		taula = new JTable(files, columnes);
		vPane.add(taula);
		JScrollPane panel = new JScrollPane(taula);
		vPane.add(panel, BorderLayout.CENTER);

	}

	public static String getTextValue(Element ele, String tagName) {
		String textVal = null;
		NodeList nl = ele.getElementsByTagName(tagName);
		if (nl != null && nl.getLength() > 0) {
			Element el = (Element) nl.item(0);
			if (el.getFirstChild().getNodeValue() != null)
				textVal = el.getFirstChild().getNodeValue();
		}

		return textVal;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == elimina) {
			DefaultTableModel model = (DefaultTableModel) taula.getModel();
			model.removeRow(taula.getSelectedRow());
		} else if (e.getSource() == afegeix) {
			NovaVisita nv = new NovaVisita();
			DefaultTableModel model3 = (DefaultTableModel) taula.getModel();
			int f = taula.getRowCount();
			for (int i = 0; f > i; i++) {
				model3.removeRow(0);
			}
			TaulerControl.frame.remove(vPane);
			nv.repaint();
		}

	}

}
