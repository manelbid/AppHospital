import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.mxrck.autocompleter.TextAutoCompleter;

/**
 * 
 * @author iam20484311
 * @param
 * @return
 * 
 *         Creates a new visit
 *
 */

public class NovaVisita extends JFrame implements ActionListener, FocusListener {

	JButton ok;
	JButton cancela;
	JDesktopPane visitaPane;
//	JPanel panel = new JPanel();
	JTextField pacient;
	JTextField metge;
	JTextField dat;
	JComboBox<String> hora;
	Calendar cal;
	TextAutoCompleter pacients;
	TextAutoCompleter metges;

	public NovaVisita() {
		super("TaulerControl");

		visitaPane = new JDesktopPane();
//		BoxLayout layout = new BoxLayout(visitaPane, BoxLayout.Y_AXIS);
		GroupLayout layout = new GroupLayout(visitaPane);
		visitaPane.setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();

		JLabel paci = new JLabel("Pacient");
		// paci.setBounds(90, 40, 140, 15);
		JLabel met = new JLabel("Metge");
		// met.setBounds(300, 40, 140, 15);
		JLabel data = new JLabel("Data");
		// data.setBounds(90, 100, 140, 15);
		JLabel horari = new JLabel("Hora");
		// horari.setBounds(300, 100, 140, 15);

		pacient = new JTextField();
		// pacient.setBounds(90, 60, 140, 20);
		metge = new JTextField();
		// metge.setBounds(300, 60, 140, 20);
		
		pacients = new TextAutoCompleter( pacient );
		metges = new TextAutoCompleter( metge );
		try {
			File arxiu = new File("src/dades2.xml");
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(arxiu);
			doc.getDocumentElement().normalize();
			NodeList nodeLst = doc.getElementsByTagName("visita");

			for (int s = 0; s < nodeLst.getLength(); s++) {
				
				Node fstNode = nodeLst.item(s);

				if (fstNode.getNodeType() == Node.ELEMENT_NODE) {

					Element fstElmnt = (Element) fstNode;

					String cognom1 = getTextValue(fstElmnt, "cognom1");
					String cognom2 = getTextValue(fstElmnt, "cognom2");
					String nom = getTextValue(fstElmnt, "nom");
					pacients.addItem(cognom1 + " i " + cognom2 + ", " + nom);
					
					String nomMet = getTextValue(fstElmnt, "metge");
					metges.addItem(nomMet);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		dat = new JTextField();
		// dat.setBounds(90, 120, 140, 20);
		dat.addFocusListener(this);

		hora = new JComboBox<String>();
		// hora.setBounds(300, 120, 140, 20);
		hora.addItem("");
		hora.addItem("08:00");
		hora.addItem("08:30");
		hora.addItem("09:00");

//		 visitaPane.add(pacient);
		// visitaPane.add(metge);
//		 visitaPane.add(paci);
		// visitaPane.add(met);
		// visitaPane.add(data);
		// visitaPane.add(dat);
		// visitaPane.add(hora);
		// visitaPane.add(horari);

		hGroup.addGroup(layout.createParallelGroup().addComponent(paci)
				.addComponent(met).addComponent(horari).addComponent(data));
		hGroup.addGroup(layout.createParallelGroup().addComponent(pacient)
				.addComponent(metge).addComponent(hora).addComponent(dat));
		layout.setHorizontalGroup(hGroup);

		GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();

		vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE)
				.addComponent(paci).addComponent(pacient));
		vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE)
				.addComponent(met).addComponent(metge));
		vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE)
				.addComponent(horari).addComponent(hora));
		vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE)
				.addComponent(data).addComponent(dat));

		layout.setVerticalGroup(vGroup);
		
		TaulerControl.frame.getContentPane().add(visitaPane);
		TaulerControl.frame.setVisible(true);

		ok = new JButton("OK");
		ok.addActionListener(this);
		ok.setBounds(750, 650, 100, 25);
		visitaPane.add(ok);

		cancela = new JButton("cancel·la");
		cancela.addActionListener(this);
		cancela.setBounds(650, 650, 100, 25);
		visitaPane.add(cancela);
	}
	
	public static String getTextValue(Element ele, String tagName) {
		String textVal = null;
		NodeList nl = ele.getElementsByTagName(tagName);
		if (nl != null && nl.getLength() > 0) {
			Element el = (Element) nl.item(0);
			if (el.getFirstChild().getNodeValue() != null)
				textVal = el.getFirstChild().getNodeValue();
		}

		return textVal;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == ok) {
			if (pacient.getText().equals("")) {
				TaulerControl.frame.remove(visitaPane);
				Visites visita = new Visites();
			} else {
				Vector fila = new Vector();
				fila.add(pacient.getText());
				fila.add(metge.getText());
				fila.add(hora.getSelectedItem());
				Visites.files.add(fila);
				TaulerControl.frame.remove(visitaPane);
				Visites visita = new Visites();
			}
		} else if (e.getSource() == cancela) {
			TaulerControl.frame.remove(visitaPane);
			TaulerControl.frame.remove(Visites.vPane);
			Visites visita = new Visites();
		}
	}

	@Override
	public void focusGained(FocusEvent e) {
		cal = new Calendar();
		cal.setVisible(true);
		cal.setBounds(400, 300, 400, 300);
	}

	@Override
	public void focusLost(FocusEvent e) {
		dat.setFocusable(false);

	}
}
