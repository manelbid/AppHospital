import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

/**
 * 
 * @author iam20484311
 * @param
 * @return
 * 
 * Shows help information
 *
 */
public class Ajuda extends JFrame {
	
	public static JScrollPane scrollPane;
	public static JTextPane txt;
	
	public Ajuda(){
		super("TaulerControl");
		scrollPane = new JScrollPane();
		TaulerControl.frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
		TaulerControl.frame.setVisible(true);

		txt = new JTextPane();
		txt.setText("\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac libero facilisis sapien fermentum auctor. Aliquam ullamcorper ligula sed metus dapibus, nec egestas lorem hendrerit. Ut tortor lorem, tincidunt et leo quis, fringilla lacinia leo. In lobortis felis vel dolor fringilla, eget auctor tellus semper. Vestibulum accumsan sed ligula sit amet auctor. Vivamus consectetur, arcu id tempor gravida, justo neque dignissim est, nec faucibus turpis sapien et massa. Phasellus ultricies sed nulla et dictum. Nulla semper, velit ac porta suscipit, lectus felis sollicitudin tellus, ac gravida purus enim nec mauris. Nulla purus orci, euismod a urna vel, congue eleifend nibh. Sed egestas nulla eu facilisis vulputate.\n\nIn ac pulvinar lectus. Nullam quis dignissim sem. Praesent vel magna molestie, vulputate risus ut, euismod nulla. Phasellus et libero purus. Sed id tortor massa. Nam quis justo vel arcu interdum dapibus. Duis mattis purus et urna vestibulum laoreet.\n\nInteger vel nunc ornare ex pellentesque viverra nec eu nisl. Etiam vitae felis dignissim, lobortis dolor in, dapibus tortor. Donec eleifend urna non massa condimentum, sed molestie magna volutpat. Suspendisse id pharetra diam. Phasellus maximus sapien at pulvinar egestas. Sed lobortis felis vitae lectus rutrum, quis tincidunt neque ultrices. Donec mauris ante, placerat at suscipit ac, facilisis non lectus. Suspendisse tempor vulputate purus, lacinia ultricies sapien blandit id. Cras dignissim libero tellus, sed egestas dolor condimentum convallis.\n\nMaecenas orci quam, elementum eget dui mollis, consectetur ornare ex. Aenean interdum commodo metus. Vivamus ullamcorper ultrices nulla, sed varius orci ultricies non. Aliquam quis rutrum ipsum. Nulla ultricies mi sed sapien commodo, at rutrum odio commodo. In rutrum nisi justo, at semper lorem dignissim et. Pellentesque et eleifend lorem. Maecenas gravida, lacus a varius viverra, sapien dolor pretium tortor, vel posuere dui dui vel lacus. Ut eu quam elementum, scelerisque diam eget, dapibus urna.\n\nInteger fringilla pellentesque nulla, id vulputate turpis euismod sed. Fusce dictum mauris quis erat feugiat, non feugiat purus auctor. Nullam eget leo id arcu volutpat semper et non massa. Phasellus tristique, magna vel dapibus semper, est nisi volutpat libero, vel laoreet ipsum tellus ut magna. Curabitur maximus, sem eu faucibus vulputate, lacus odio auctor felis, vel pellentesque nisi dolor non ante. Quisque pellentesque eros sit amet est venenatis tristique sit amet in enim. Morbi cursus rhoncus nibh, nec laoreet odio suscipit quis. Curabitur risus tellus, gravida sed tincidunt at, ornare id felis. Nullam nec porta ante, non suscipit justo. Proin in efficitur erat, ut viverra eros. Maecenas vulputate neque neque, a gravida quam varius nec. Etiam porttitor scelerisque quam, accumsan porttitor dui condimentum sed. Maecenas in turpis in nulla efficitur blandit vitae vel mauris. In ac leo erat. ");
		scrollPane.setViewportView(txt);
	}

}
