import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * 
 * @author iam20484311
 * @param
 * @return
 * 
 * Shows patients data
 *
 */

public class Dades extends JFrame implements ActionListener {

	JButton elimina;
	JButton afegeix;
	static JDesktopPane dPane;
	static Vector fila2 = new Vector();
	static Vector fila4 = new Vector();
	static Vector files = new Vector();
	JTable taula;

	public Dades() {
		super("TaulerControl");

		dPane = new JDesktopPane();
		dPane.setLayout(new BorderLayout());
		TaulerControl.frame.getContentPane().add(dPane);
		TaulerControl.frame.setVisible(true);

		elimina = new JButton("elimina");
		elimina.addActionListener(this);
		elimina.setBounds(850, 650, 100, 25);
		dPane.add(elimina);

		afegeix = new JButton("afegeix");
		afegeix.addActionListener(this);
		afegeix.setBounds(650, 650, 100, 25);
		dPane.add(afegeix);
		
		JButton modifica = new JButton("modifica");
		modifica.addActionListener(this);
		modifica.setBounds(750, 650, 100, 25);
		dPane.add(modifica);

		Vector columnes = new Vector();
		
		columnes.add("Cognoms");
		columnes.add("Nom");
		columnes.add("DNI");
		columnes.add("Data Naixement");
		columnes.add("Targeta");
		columnes.add("estat");
		columnes.add("Al·lèrgies");

		
		Vector fila = new Vector();
		
		Vector fila3 = new Vector();

		fila.add("Garcia");
		fila.add("Pepe");
		fila.add("12345678P");
		fila.add("31/12/1980");
		fila.add("PEGA 000 000");
		fila.add("1");	

		files.add(fila);
		files.add(fila2);
		files.add(fila3);

		taula = new JTable(files, columnes);
		dPane.add(taula);
		JScrollPane panel = new JScrollPane(taula);
		dPane.add(panel, BorderLayout.CENTER);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == elimina) {
			DefaultTableModel model = (DefaultTableModel)taula.getModel();
			model.removeRow(taula.getSelectedRow());
		} else if (e.getSource() == afegeix) {
			CreaPacient cp;
			try {
				cp = new CreaPacient();
				TaulerControl.frame.remove(dPane);
				cp.repaint();
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}

	}

}