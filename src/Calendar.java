import java.awt.FlowLayout;

import javax.swing.JFrame;

import org.freixas.jcalendar.JCalendar;


/**
 * 
 * @author iam20484311
 * @param
 * @return
 * 
 * Shows a calendar to pick dates
 *
 */

public class Calendar extends JFrame {
public Calendar()
    {
	
      JCalendar calEjemplo1=new JCalendar();
      this.add(calEjemplo1);
      this.setLayout(new FlowLayout());
      this.setSize(400, 300);
 
      setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    }
 
    public static void main(String args[]) {
        Calendar obj = new Calendar();
        obj.setVisible(true);
    }
}