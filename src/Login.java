import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * 
 * @author iam20484311
 * @param
 * @return
 *
 */

public class Login implements ActionListener {

	private JFrame frame;
	private JPasswordField pwdPass;
	private JTextField tf;

	JLabel info;
	JLabel picture;
	JButton btnClear;
	JButton btnOk;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(300, 300, 450, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBackground(Color.WHITE);
		frame.getContentPane().add(desktopPane, BorderLayout.CENTER);

		pwdPass = new JPasswordField();
		pwdPass.setBounds(205, 151, 133, 27);
		desktopPane.add(pwdPass);
		pwdPass.setToolTipText("Introdueix contrassenya");

		JLabel lblContrassenya = new JLabel("Contrassenya");
		lblContrassenya.setBounds(82, 157, 105, 15);
		desktopPane.add(lblContrassenya);

		JLabel lblUsuari = new JLabel("Usuari");
		lblUsuari.setBounds(82, 101, 105, 15);
		desktopPane.add(lblUsuari);

		tf = new JTextField("");
		tf.setBounds(205, 95, 133, 27);
		desktopPane.add(tf);
		tf.setToolTipText("Introdueix nom d'usuari");

		btnClear = new JButton("Esborra les dades");
		btnClear.setBounds(57, 275, 172, 25);
		desktopPane.add(btnClear);
		btnClear.addActionListener(this);
		btnClear.setToolTipText("Neteja el formulari");

		btnOk = new JButton("OK");
		btnOk.setBackground(new Color(60, 179, 113));
		btnOk.setBounds(325, 259, 68, 56);
		desktopPane.add(btnOk);
		btnOk.addActionListener(this);
		btnOk.setToolTipText("Valida les dades");
		btnOk.setMnemonic(KeyEvent.VK_ENTER);

		info = new JLabel("");
		info.setHorizontalAlignment(SwingConstants.CENTER);
		info.setForeground(Color.RED);
		info.setBounds(76, 219, 292, 15);
		desktopPane.add(info);

		// Add a logo
		picture = new JLabel("");
		ImageIcon image = new ImageIcon("");
		picture.setBounds(23, 12, 105, 77);
		picture.setIcon(image);
		desktopPane.add(picture);

		Image icon = new ImageIcon(getClass().getResource(
				"images/hospital-icon.gif")).getImage();
		image.setImage(icon);
	}

	@Override
	public void actionPerformed(ActionEvent login) {
		Map<String, String> data = new LinkedHashMap<String, String>();
		data.put("supervisor1", "super");
		data.put("infermeria1", "infe");
		data.put("infermeria2", "infer");

		// Clear data
		if (login.getSource() == btnClear) {
			tf.setText("");
			pwdPass.setText("");
			info.setText("");
		} else {
			// Validate the information
			Iterator<String> it = data.keySet().iterator();
			Iterator<String> it2 = data.values().iterator();
			boolean continuar = true;
			while (it.hasNext() && continuar) {
				String user = it.next();
				String pass = it2.next();
				if (user.equals(tf.getText()) && pass.equals(pwdPass.getText())) {
					info.setText("Validació correcta");
					continuar = false;
					// Open new window
					TaulerControl tc = new TaulerControl();
					tc.frame.setVisible(true);
					
				} else {
					info.setText("Usuari o contrassenya incorrectes!");
				}
			}
		}
	}
}
