import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.MutableComboBoxModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class Medicaments extends JFrame implements ActionListener {

	static JDesktopPane mPane = new JDesktopPane();
	static JPanel pane = new JPanel();
	static JPanel pane2 = new JPanel();
	static JPanel pane3 = new JPanel();
	final JButton rebutjat;
	final JButton pres;
	JTextField nom = new JTextField();
	JTextField quant = new JTextField();
	JButton afeg = new JButton("Afegeix");
	JComboBox<String> medicam;
	JTable taula;
	int parac = 2;
	int ibu = 2;
	int nou;
	String nouMedicament = "";

	public Medicaments() {
		super("TaulerControl");
		mPane.setLayout(new BorderLayout());
		mPane.add(pane, BorderLayout.WEST);
		mPane.add(pane2, BorderLayout.CENTER);
		mPane.add(pane3, BorderLayout.EAST);

		JLabel admin = new JLabel("   Administra medicament   ");
		pane.add(admin);

		medicam = new JComboBox<String>();
		medicam.addItem("medicaments");
		medicam.addItem("Paracetamol " + parac);
		medicam.addItem("Ibuprofè " + ibu);
		pane.add(medicam);
		medicam.addActionListener(this);
		
		MouseListener m = new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {

			}

			@Override
			public void mouseEntered(MouseEvent e) {

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				pres.setVisible(true);
				rebutjat.setVisible(true);

			}
		};

		// try {
		// File xml = new File("src/dades3.xml");
		// DocumentBuilderFactory dbFactory = DocumentBuilderFactory
		// .newInstance();
		//
		// Document doc = dbFactory.newDocumentBuilder().parse(xml);
		//
		// doc.getDocumentElement().normalize();
		//
		// NodeList lista = doc.getElementsByTagName("nom");
		// NodeList lista2 = doc.getElementsByTagName("quantitat");
		//
		// for (int i = 0; i < lista.getLength(); i++) {
		//
		// Node nodo = lista.item(i);
		// Node nodo2 = lista2.item(i);
		//
		// if (nodo.getNodeType() == Node.ELEMENT_NODE) {
		// medicam.addItem(nodo.getTextContent() + " " +
		// nodo2.getTextContent());
		// }
		// }
		// } catch (Exception e) {
		// e.printStackTrace();
		// }

		JLabel afegeix = new JLabel("   Afegeix medicament   ");
		pane.add(afegeix);
		JLabel n = new JLabel("Nom");
		JLabel q = new JLabel("Quantitat");
		pane.add(n);
		pane.add(nom);
		pane.add(q);
		pane.add(quant);
		afeg.addActionListener(this);
		pane.add(afeg);

		pane.setLayout(new GridLayout(15, 1));
		pane3.setLayout(new GridLayout(15, 1));

		Vector fila = new Vector();
		Vector fila2 = new Vector();
		Vector files = new Vector();
		files.add(fila);
		files.add(fila2);
		fila.add("1");
		fila.add("Pich i Poch, Joan");
		fila2.add("2");
		fila2.add("Poch i Pich, Joana");

		Vector columnes = new Vector();

		columnes.add("Habitació");
		columnes.add("Nom Complet");

		taula = new JTable(files, columnes);

		pane2.add(taula);
		JScrollPane panel = new JScrollPane(taula);
		taula.setVisible(false);
		pane2.add(panel, BorderLayout.CENTER);

		pres = new JButton("Medicament pres");
		rebutjat = new JButton("Medicament rebutjat");
		rebutjat.setVisible(false);
		pres.setVisible(false);
		pres.addActionListener(this);
		rebutjat.addActionListener(this);
		pane3.add(pres);
		pane3.add(rebutjat);

		taula.addMouseListener(m);

		TaulerControl.frame.getContentPane().add(mPane);
		TaulerControl.frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == medicam) {
			taula.setVisible(true);
		} else if (e.getSource() == pres) {
			if (medicam.getSelectedItem().equals("Paracetamol " + parac)) {
				if (parac > 0) {
					parac--;
				}
			} else if (medicam.getSelectedItem().equals("Ibuprofè " + ibu)) {
				if (ibu > 0) {
					ibu--;
				}
			} else if (medicam.getSelectedItem().equals(
					nouMedicament + " " + nou)) {
				if (nou > 0) {
					nou--;
				}
			}
			medicam.removeAllItems();

			medicam.addItem("medicaments");
			if (parac > 0) {
				medicam.addItem("Paracetamol " + parac);
			}
			if (ibu > 0) {
				medicam.addItem("Ibuprofè " + ibu);
			}
			if (nouMedicament.equals("")) {
			} else if (nou > 0) {
				medicam.addItem(nouMedicament + " " + nou);
			}
			taula.setVisible(false);
			pres.setVisible(false);
			rebutjat.setVisible(false);
		} else if (e.getSource() == rebutjat) {
			taula.setVisible(false);
			pres.setVisible(false);
			rebutjat.setVisible(false);
		} else if (e.getSource() == afeg) {
			try {
				nouMedicament = nom.getText();
				nou = Integer.parseInt(quant.getText());
			} catch (NumberFormatException nfe) {

			}
			if (nouMedicament.equals("")) {
			} else {
				medicam.removeAllItems();
				medicam.addItem("medicaments");
				if (parac > 0) {
					medicam.addItem("Paracetamol " + parac);
				}
				if (ibu > 0) {
					medicam.addItem("Ibuprofè " + ibu);
				}
				medicam.addItem(nouMedicament + " " + nou);
				nom.setText(null);
				quant.setText(null);
			}
		}
	}
}
